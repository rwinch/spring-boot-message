/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sample.message;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sample.message.MessageTestUtils.createMessage;

/**
 * Runs end to end tests for Message.
 *
 * @author Rob Winch
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class MessagesEndToEndTest {
	@MockBean
	MessageRepository messages;

	@Autowired
	MockMvc mockMvc;

	@Test
	public void findOneWhenMessageFoundThenOk() throws Exception {
		// expect that MessageRepository will be invoked and return a value
		Message expected = createMessage();
		when(this.messages.findOne(expected.getId())).thenReturn(expected);

		// run the test
		this.mockMvc.perform(get("/messages/{id}", expected.getId()))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", Matchers.is(1)))
				.andExpect(jsonPath("$.text", Matchers.is(expected.getText())));

		// verify that the MessageRepository.findOne was invoked with the correct argument
		verify(this.messages).findOne(expected.getId());
	}

	@Test
	public void findOneWhenMessageFoundNotFoundThen404() throws Exception {
		// no expectations set on the mock, so it will default to returning null
		long id = 1L;

		// run the test
		this.mockMvc.perform(get("/messages/{id}", id)).andExpect(status().isNotFound())
				.andExpect(content().string(""));

		// verify that the MessageRepository.findOne was invoked with the correct argument
		verify(this.messages).findOne(id);
	}
}
