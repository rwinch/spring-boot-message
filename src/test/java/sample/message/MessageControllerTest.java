/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sample.message;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Runs Unit tests for a Message.
 *
 * @author Rob Winch
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class MessageControllerTest {
	@Mock
	MessageRepository messages;

	MessageController controller;

	@Before
	public void setup() {
		this.controller = new MessageController(this.messages);
	}

	@Test
	public void findOneWhenMessageFoundThenOk() {
		// expect that MessageRepository will be invoked and return a value
		Message expected = MessageTestUtils.createMessage();
		when(this.messages.findOne(expected.getId())).thenReturn(expected);

		// run the test
		ResponseEntity<Message> entity = this.controller.findOne(expected.getId());
		Message body = entity.getBody();

		// verify that the result matches the expected value
		assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(body.getId()).isEqualTo(expected.getId());
		assertThat(body.getText()).isEqualTo(expected.getText());

		// verify that the MessageRepository.findOne was invoked with the correct argument
		verify(this.messages).findOne(expected.getId());
	}

	@Test
	public void findOneWhenMessageFoundNotFoundThen404() {
		// no expectations set on the mock, so it will default to returning null
		long id = 1L;

		ResponseEntity<Message> entity = this.controller.findOne(id);

		assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(entity.getBody()).isNull();

		// verify that the MessageRepository.findOne was invoked with the correct argument
		verify(this.messages).findOne(id);
	}

}
