/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sample.message;

import java.net.URI;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Runs integration tests for Message REST API.
 *
 * @author Rob Winch
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MessagesIntegrationTest {
	@Autowired
	TestRestTemplate rest;

	@Test
	public void messagesWhenId1ThenGetHelloSpringBoot() {
		long id = 100;
		Message message = this.rest.getForEntity("/messages/{id}", Message.class, id)
				.getBody();

		assertThat(message).isNotNull();
		assertThat(message.getText()).isEqualTo("Hello Spring Boot!");
		assertThat(message.getTo()).isEqualTo("rwinch@users.noreply.github.com");
	}

	/**
	 * Spring Data REST support automatically exposes the endpoints for us to manage the
	 * Message (we didn't really need MessageController), so we did not need to create the
	 * code to do this. However, it is good to ensure everything is working properly using
	 * an integration test.
	 */
	@Test
	public void messagesCreate() {
		Message toCreate = MessageTestUtils.createMessage();

		ResponseEntity<Message> created = this.rest.postForEntity("/messages/", toCreate,
				Message.class);

		assertThat(created).isNotNull();
		assertThat(created.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(created.getBody().getText()).isEqualTo(toCreate.getText());

		URI createdUri = created.getHeaders().getLocation();

		Message getForEntity = this.rest.getForEntity(createdUri, Message.class)
				.getBody();
		assertThat(getForEntity.getText()).isEqualTo(toCreate.getText());
	}
}
